﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Spawner::Start()
extern void Spawner_Start_m61D97BD980BD1B1877634A1E7626E47418D5D6D8 (void);
// 0x00000002 System.Collections.IEnumerator Spawner::createObject()
extern void Spawner_createObject_m1302435D9B0C0CE49C2CF5F6C6BD90786ED3AF64 (void);
// 0x00000003 System.Void Spawner::.ctor()
extern void Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C (void);
// 0x00000004 System.Void Spawner/<createObject>d__2::.ctor(System.Int32)
extern void U3CcreateObjectU3Ed__2__ctor_m1CE3065650F017EC68D9136B2E93321C35E25C40 (void);
// 0x00000005 System.Void Spawner/<createObject>d__2::System.IDisposable.Dispose()
extern void U3CcreateObjectU3Ed__2_System_IDisposable_Dispose_m2077C2F6A68DDED1CF296CD32862AA2C8B0B695F (void);
// 0x00000006 System.Boolean Spawner/<createObject>d__2::MoveNext()
extern void U3CcreateObjectU3Ed__2_MoveNext_mEBE39B2A018511B9FDCF13CDD7A42DCA4EB8487E (void);
// 0x00000007 System.Object Spawner/<createObject>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcreateObjectU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86813A812FBEF6F466C7CC696CB38F9CFB0A43D5 (void);
// 0x00000008 System.Void Spawner/<createObject>d__2::System.Collections.IEnumerator.Reset()
extern void U3CcreateObjectU3Ed__2_System_Collections_IEnumerator_Reset_m73F156F0D2C73ABE93A19ACA5EBD41464299D4D4 (void);
// 0x00000009 System.Object Spawner/<createObject>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CcreateObjectU3Ed__2_System_Collections_IEnumerator_get_Current_m5DB6B557BD8E9DF3EF9334118E7FC9BE637D0757 (void);
// 0x0000000A System.Void Swipe::Start()
extern void Swipe_Start_m67570B95FB0889F6D2990E8731474EE3E76929D0 (void);
// 0x0000000B System.Void Swipe::Update()
extern void Swipe_Update_m5B8885067E3CB784950DB863B1BDDDA26652D0BD (void);
// 0x0000000C System.Void Swipe::OnTriggerEnter(UnityEngine.Collider)
extern void Swipe_OnTriggerEnter_m78438654E4B47D0C2B3EE5006D29C5598F7194AC (void);
// 0x0000000D System.Void Swipe::.ctor()
extern void Swipe__ctor_m97873674A6FB4BD351B24309C91C278A784F013B (void);
// 0x0000000E System.Void SwipeActivator::OnTriggerEnter(UnityEngine.Collider)
extern void SwipeActivator_OnTriggerEnter_m97CD64C3765F17760FA7295A9AE93D78E30B3AB5 (void);
// 0x0000000F System.Collections.IEnumerator SwipeActivator::colliderActivator()
extern void SwipeActivator_colliderActivator_m4DE4A9928E668510A3A970B4E8E51B80D7CA490F (void);
// 0x00000010 System.Void SwipeActivator::.ctor()
extern void SwipeActivator__ctor_m88AC13F2A9E5F36A84C66EDD0AA038083FA9EF65 (void);
// 0x00000011 System.Void SwipeActivator/<colliderActivator>d__1::.ctor(System.Int32)
extern void U3CcolliderActivatorU3Ed__1__ctor_m8329D7EB47C80FE85C7C04C3FDF4F756857EDDC6 (void);
// 0x00000012 System.Void SwipeActivator/<colliderActivator>d__1::System.IDisposable.Dispose()
extern void U3CcolliderActivatorU3Ed__1_System_IDisposable_Dispose_m4180B71DC2E9CDFA7773BEA6A833979C1DA03478 (void);
// 0x00000013 System.Boolean SwipeActivator/<colliderActivator>d__1::MoveNext()
extern void U3CcolliderActivatorU3Ed__1_MoveNext_m3B5CC5BEC2BCEF5AAED3DA8BA117EF2299C77B0E (void);
// 0x00000014 System.Object SwipeActivator/<colliderActivator>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcolliderActivatorU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF76A51899BEE651CEAEA5C1164ACC61004754E41 (void);
// 0x00000015 System.Void SwipeActivator/<colliderActivator>d__1::System.Collections.IEnumerator.Reset()
extern void U3CcolliderActivatorU3Ed__1_System_Collections_IEnumerator_Reset_m53D4C5EB57929156F7488F4AE2DD16133D1CD61C (void);
// 0x00000016 System.Object SwipeActivator/<colliderActivator>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CcolliderActivatorU3Ed__1_System_Collections_IEnumerator_get_Current_mD5CFCF1631F6F16321DA6580F2475A0A4E3DE7E7 (void);
static Il2CppMethodPointer s_methodPointers[22] = 
{
	Spawner_Start_m61D97BD980BD1B1877634A1E7626E47418D5D6D8,
	Spawner_createObject_m1302435D9B0C0CE49C2CF5F6C6BD90786ED3AF64,
	Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C,
	U3CcreateObjectU3Ed__2__ctor_m1CE3065650F017EC68D9136B2E93321C35E25C40,
	U3CcreateObjectU3Ed__2_System_IDisposable_Dispose_m2077C2F6A68DDED1CF296CD32862AA2C8B0B695F,
	U3CcreateObjectU3Ed__2_MoveNext_mEBE39B2A018511B9FDCF13CDD7A42DCA4EB8487E,
	U3CcreateObjectU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86813A812FBEF6F466C7CC696CB38F9CFB0A43D5,
	U3CcreateObjectU3Ed__2_System_Collections_IEnumerator_Reset_m73F156F0D2C73ABE93A19ACA5EBD41464299D4D4,
	U3CcreateObjectU3Ed__2_System_Collections_IEnumerator_get_Current_m5DB6B557BD8E9DF3EF9334118E7FC9BE637D0757,
	Swipe_Start_m67570B95FB0889F6D2990E8731474EE3E76929D0,
	Swipe_Update_m5B8885067E3CB784950DB863B1BDDDA26652D0BD,
	Swipe_OnTriggerEnter_m78438654E4B47D0C2B3EE5006D29C5598F7194AC,
	Swipe__ctor_m97873674A6FB4BD351B24309C91C278A784F013B,
	SwipeActivator_OnTriggerEnter_m97CD64C3765F17760FA7295A9AE93D78E30B3AB5,
	SwipeActivator_colliderActivator_m4DE4A9928E668510A3A970B4E8E51B80D7CA490F,
	SwipeActivator__ctor_m88AC13F2A9E5F36A84C66EDD0AA038083FA9EF65,
	U3CcolliderActivatorU3Ed__1__ctor_m8329D7EB47C80FE85C7C04C3FDF4F756857EDDC6,
	U3CcolliderActivatorU3Ed__1_System_IDisposable_Dispose_m4180B71DC2E9CDFA7773BEA6A833979C1DA03478,
	U3CcolliderActivatorU3Ed__1_MoveNext_m3B5CC5BEC2BCEF5AAED3DA8BA117EF2299C77B0E,
	U3CcolliderActivatorU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF76A51899BEE651CEAEA5C1164ACC61004754E41,
	U3CcolliderActivatorU3Ed__1_System_Collections_IEnumerator_Reset_m53D4C5EB57929156F7488F4AE2DD16133D1CD61C,
	U3CcolliderActivatorU3Ed__1_System_Collections_IEnumerator_get_Current_mD5CFCF1631F6F16321DA6580F2475A0A4E3DE7E7,
};
static const int32_t s_InvokerIndices[22] = 
{
	875,
	853,
	875,
	752,
	875,
	870,
	853,
	875,
	853,
	875,
	875,
	759,
	875,
	759,
	853,
	875,
	752,
	875,
	870,
	853,
	875,
	853,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	22,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
